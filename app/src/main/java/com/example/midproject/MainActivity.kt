package com.example.midproject

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    private  var start = true
    private  var score = 0;
    private  var cda = 0;
    private  var againvar = true;
    private  var mogeba = 0;
    private  var wageba = 0;



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        again()
    }

    private fun init() {
        generate.setOnClickListener{
            generate()
        }
    }
    //Play again starts from
    private  fun again(){
        againButton.setOnClickListener{
            playAgain()
        }
    }



    private fun playAgain(){
        if(againvar){
            cda = 0;
            score = 0;
            start = true;
            countTry.text = "გამოყენებული ცდები: 0";
            score1.text = "შენ გაქვს 0 ქულა";
            output.text = "";
            output1.text = "";
            output2.text = "";
            output3.text = "";
            mogeba = 0;
            wageba = 0;
            wagebaBox.text = "";
            mogebaBox.text = "";
            output2.visibility = View.GONE
            output3.visibility = View.GONE

        }
    }

    // ends

    //Generate numbers
    @SuppressLint("SetTextI18n")
    private  fun generate() {
        if (start){
            val randomValues = List(4) { Random.nextInt(1, 6) }
            println(randomValues)
            cda++;
            output.text = randomValues[0].toString()
            output1.text= randomValues[1].toString()
            output2.text = randomValues[2].toString()
            output3.text= randomValues[3].toString()
            countTry.text = "გამოყენებული ცდები: $cda";
            score1.text = "შენ გაქვს $score ქულა";





        }
// თუ დაემთხვა რიცხვები +1 ქულა
        if (output2.visibility == View.GONE) {
            if (output.text === output1.text) {
                Toast.makeText(applicationContext, "კარგია +1 ქულა!", Toast.LENGTH_SHORT)
                    .show();
                score++;
                score1.text = "შენ გაქვს $score ქულა";


// დააგროვა 6 ქულა და გადავიდა შემდეგ ტურიში

                if (score == 6) {
                    Toast.makeText(
                        applicationContext,
                        "გილოცავ მოიგე - თამაში 6-მდე იყო. გადახვედი მეორე ტურში ",
                        Toast.LENGTH_LONG
                    ).show();
                    score = 0;
                    cda = 0;
                    score1.text = "შენ გაქვს 0 ქულა";
                    output.text = "";
                    output1.text = "";
                    output2.text = "";
                    mogeba++;
                    mogebaBox.text = "მოგებული ხელი $mogeba";
                    output2.visibility = View.VISIBLE


                }


// ამოეწურა ცდები და თამაში თავიდან იწყება
            } else if (cda >= 31) {
                Toast.makeText(
                    applicationContext,
                    "ამოგეწურა ცდები - თამაში მორჩა ",
                    Toast.LENGTH_LONG
                ).show();
                score = 0;
                cda = 0;
                countTry.text = "გამოყენებული ცდები: 0";
                score1.text = "შენ გაქვს 0 ქულა";
                output.text = "";
                output1.text = "";
                output2.text = "";
                wageba++;
                wagebaBox.text = "წაგებული ხელი $wageba";

            } else {

                Toast.makeText(
                    applicationContext,
                    "სამწუხაროდ არ დაემთხვა რიცხვები",
                    Toast.LENGTH_SHORT
                ).show();


            }


        }else if (output2.visibility == View.VISIBLE && output3.visibility != View.VISIBLE){
            if (output.text === output1.text && output.text === output2.text) {
                Toast.makeText(applicationContext, "კარგია +1 ქულა!", Toast.LENGTH_SHORT)
                    .show();
                score++;
                score1.text = "შენ გაქვს $score ქულა";


// დააგროვა 3 ქულა და გადავიდა კიდევ შემდეგ ტურში - დაემატა მეოთხე ყუთი

                if (score == 3) {
                    Toast.makeText(
                        applicationContext,
                        "გილოცავ მოიგე - თამაში 3-მდე იყო" ,
                        Toast.LENGTH_LONG
                    ).show();
                    score = 0;
                    cda = 0;
                    score1.text = "შენ გაქვს 0 ქულა";
                    output.text = "";
                    output1.text = "";
                    output2.text = "";
                    output3.text = "";
                    mogeba++;
                    mogebaBox.text = "მოგებული ხელი $mogeba";
                    output3.visibility = View.VISIBLE



                }


// ამოეწურა ცდები და ცდის თავიდან
            } else if (cda >= 51) {
                Toast.makeText(
                    applicationContext,
                    "ამოგეწურა ცდები ",
                    Toast.LENGTH_LONG
                ).show();
                score = 0;
                cda = 0;
                countTry.text = "გამოყენებული ცდები: 0";
                score1.text = "შენ გაქვს 0 ქულა";
                output.text = "";
                output1.text = "";
                output2.text = "";

                wageba++;
                wagebaBox.text = "წაგებული ხელი: $wageba";

            } else {

                Toast.makeText(
                    applicationContext,
                    "სამწუხაროდ არ დაემთხვა რიცხვები",
                    Toast.LENGTH_SHORT
                ).show();


            }
        }  // მესამე ტური
        else if (output3.visibility == View.VISIBLE){
            if (output.text === output1.text && output.text === output2.text && output.text === output3.text) {
                Toast.makeText(applicationContext, "კარგია +1 ქულა!", Toast.LENGTH_SHORT)
                    .show();
                score++;
                score1.text = "შენ გაქვს $score ქულა";


// დააგროვა 2 ქულა და მოიგო საბოლოოდ. შეგიძლია დაიწყო თავიდან ან გააგრძელო თამაში 4 ყუთზე

                if (score == 2) {
                    Toast.makeText(
                        applicationContext,
                        "გილოცავ მოიგე - თამაში 2-მდე იყო" ,
                        Toast.LENGTH_LONG
                    ).show();
                    score = 0;
                    cda = 0;
                    score1.text = "შენ გაქვს 0 ქულა";
                    output.text = "";
                    output1.text = "";
                    output2.text = "";
                    output3.text = "";
                    mogeba++;
                    mogebaBox.text = "მოგებული ხელი $mogeba";
                    output3.visibility = View.VISIBLE



                }


// ამოეწურა ცდები და ცდის თავიდან
            } else if (cda >= 101) {
                Toast.makeText(
                    applicationContext,
                    "ამოგეწურა ცდები - თამაში მორჩა ",
                    Toast.LENGTH_LONG
                ).show();
                score = 0;
                cda = 0;
                countTry.text = "გამოყენებული ცდები: 0";
                score1.text = "შენ გაქვს 0 ქულა";
                output.text = "";
                output1.text = "";
                output2.text = "";
                output3.text = "";
                wageba++;
                wagebaBox.text = "წაგებული ხელი: $wageba";

            } else {

                Toast.makeText(
                    applicationContext,
                    "სამწუხაროდ არ დაემთხვა რიცხვები",
                    Toast.LENGTH_SHORT
                ).show();


            }
        }

    }

}


